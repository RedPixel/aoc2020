from collections import defaultdict
with open('day10.txt', 'r') as f:
    data = f.read()
jolts = [0] + [int(jolt) for jolt in data.splitlines()]
jolts.sort()
jolts.reverse()

paths=defaultdict(int)
paths[jolts[0]]=1
for n,jolt in enumerate(jolts):
    for i in range(1,4):
        if (jolts[n-i]-jolt <= 3):
            paths[jolt] += paths[jolts[n-i]]
print(paths[0])