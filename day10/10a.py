with open('day10.txt', 'r') as f:
    data = f.read()
jolts = [int(jolt) for jolt in data.splitlines()]

jolts.sort()
jolts.append(jolts[-1]+3) # append device
jolts.insert(0,0) # prepend outlet

ones = 0
threes = 0

for i in range(len(jolts)-1):
    if jolts[i+1]-jolts[i] == 1:
        ones += 1
    elif jolts[i+1]-jolts[i] == 3:
        threes += 1
    elif jolts[i+1]-jolts[i] > 3:
        break
print(ones, threes, ones*threes)