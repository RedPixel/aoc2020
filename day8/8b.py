from parse import *
from parse import compile # overrides python compile, therefore not in *

def main():
    with open('day8.txt', 'r') as f:
        ins = f.read()
    ins = ins.splitlines()
    p = compile('{} {}')
    
    for swap in range(len(ins)):
        op, num = p.parse(ins[swap])
        
        if op == 'acc':
            continue

        visited = set()
        pc = 0
        acc = 0
        
        while not pc in visited:
            if pc == len(ins):
                print("program terminated by swapping " + str(swap))
                print(acc)
                return
            
            visited.add(pc) 
            op, num = p.parse(ins[pc])
            num = int(num)

            if pc==swap:
                if op=='jmp':
                    op='nop'
                else:
                    op='jmp'

            if op == 'acc':
                acc += num
                pc += 1
            elif op == 'jmp':
                pc += num
            elif op == 'nop':
                pc += 1
        
if __name__ == "__main__":
    main()