from parse import *
from parse import compile # overrides python compile, therefore not in *

def main():
    with open('day8.txt', 'r') as f:
        ins = f.read()
    ins = ins.splitlines()
    
    visited = set()
    pc = 0
    acc = 0
    p = compile('{} {}')

    while not pc in visited:
        visited.add(pc) 
        op, num = p.parse(ins[pc])
        num = int(num)

        # print(pc, op, num)
        
        if op == 'acc':
            acc += num
            pc += 1
        elif op == 'jmp':
            pc += num
        elif op == 'nop':
            pc += 1
    
    print(acc)


if __name__ == "__main__":
    main()