with open('day12.txt', 'r') as f:
    actions = f.read().splitlines()

dirs = {'N': 1, 'S': -1, 'E': 1j, 'W': -1j}
p,w = 0,1+10j
for a in actions:
    d,v = a[0],int(a[1:])
    if d in dirs:
        w += v*dirs[d]
    elif d == 'L':
        w *= 1j**(-v/90)
    elif d == 'R':
        w *= 1j**( v/90)
    elif d == 'F':
        p += v*w
print(abs(p.real)+abs(p.imag))
