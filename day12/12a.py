with open('day12.txt', 'r') as f:
    actions = f.read().splitlines()

dirs = {'N': 1, 'S': -1, 'E': 1j, 'W': -1j}
p,r = 0,1j
for a in actions:
    d,v = a[0],int(a[1:])
    if d in dirs:
        p += v*dirs[d]
    elif d == 'L':
        r *= 1j**(-v/90)
    elif d == 'R':
        r *= 1j**( v/90)
    elif d == 'F':
        p += v*r
print(abs(p.real)+abs(p.imag))
