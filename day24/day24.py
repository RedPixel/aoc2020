from collections import defaultdict
import re
import time

start_time = time.perf_counter_ns()

m = {'e': (1,0), 'se':  (0,-1), 'sw': (-1,-1), 'w': (-1,0), 'nw': (0,1), 'ne': (1,1)}

with open('day24.txt', 'r') as f:
    lines = f.read().splitlines()

tiles = defaultdict(int)
for line in lines:
    dirs = re.findall('e|se|sw|w|nw|ne', line)
    x,y = 0,0
    for d in dirs:
        dx,dy = m[d]
        x += dx
        y += dy
    tiles[(x,y)] ^= 1
print(sum(v for v in tiles.values()))

for _ in range(100):
    poss = set((x+dx, y+dy) for x,y in tiles.keys() for dx,dy in m.values())
    new = defaultdict(int)
    for x, y in poss:
        n = sum(tiles[x+dx, y+dy] for dx,dy in m.values())
        new[x,y] = tiles[(x,y)]
        if tiles[(x,y)]:
            if n not in [1, 2]:
                new[x,y] = 0
        elif n == 2:
            new[x,y] = 1            
    tiles = new
print(sum(v for v in tiles.values()))
end_time = time.perf_counter_ns()
print(f'Calculation took {(end_time - start_time) / 1e9} seconds.')
