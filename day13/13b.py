with open('day13.txt', 'r') as f:
    data = f.read().splitlines()

ids = [int(x) if x!='x' else 1 for x in data[1].split(',')]
t,a = 0,1
for i,id in enumerate(ids):
    while (t+i)%id: 
        t+=a
    a*=id
print(t)
