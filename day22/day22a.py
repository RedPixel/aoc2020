def play(deck1, deck2):
    while len(deck1)!=0 and len(deck2)!=0:
        card1,card2 = deck1.pop(0),deck2.pop(0)
        if card1 > card2:
            deck1 += [card1,card2]
        else:
            deck2 += [card2,card1]
    return deck1 + deck2

def score(deck):
    return sum([c*(len(deck)-i) for i,c in enumerate(deck)])

with open('day22.txt', 'r') as f:
    decks = f.read().split('\n\n')
deck1 = [int(n) for n in decks[0].splitlines()[1:]]
deck2 = [int(n) for n in decks[1].splitlines()[1:]]
print(score(play(deck1,deck2)))