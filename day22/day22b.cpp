#include <iostream>
#include <deque>
 
int main()
{
    // Create a deque containing integers
    std::vector<int> deck1 = {18,50,9,4,25,37,39,40,29,6,41,28,3,11,31,8,1,38,33,30,42,15,26,36,43};
    std::vector<int> deck2 = {32,44,19,47,12,48,14,2,13,10,35,45,34,7,5,17,46,21,24,49,16,22,20,27,23};
 
    // Add an integer to the beginning and end of the deque
    d.push_front(13);
    d.push_back(25);
 
    // Iterate and print values of deque
    for(int n : d) {
        std::cout << n << '\n';
    }
}

std::deque<int> play(std::deque<int> deck1, std::deque<int> deck2){
    while(deck1.size() != 0 && deck1.size()){

    }
}


def rec_play(deck1,deck2):
    plays = set()
    while len(deck1)!=0 and len(deck2)!=0:
        this_round = tuple(deck1+[0]+deck2)
        if this_round in plays:
            return 1, deck1

        plays.add(this_round)
        card1,card2 = deck1.pop(0),deck2.pop(0)
        if card1 <= len(deck1) and card2 <= len(deck2):
            winner, _= rec_play(deck1[:card1], deck2[:card2])
        else:
            winner = 1 if card1 > card2 else 2

        if winner == 1:
            deck1 += [card1,card2]
        else:
            deck2 += [card2,card1]
    if deck1:
        return (1, deck1)
    else:
        return (2, deck2)

def score(deck):
    return sum([c*(len(deck)-i) for i,c in enumerate(deck)])

start_time = time.time()

with open('day22.txt', 'r') as f:
    decks = f.read().split('\n\n')

deck1 = [int(n) for n in decks[0].splitlines()[1:]]
deck2 = [int(n) for n in decks[1].splitlines()[1:]]
_, deck = rec_play(deck1,deck2)
print(score(deck))

print("--- %s seconds ---" % (time.time() - start_time))
