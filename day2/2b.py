with open('day2.txt', 'r') as f:
    data = f.read()
lines = data.splitlines()
tokens = [x.split() for x in lines]
good = 0
for entry in tokens:
    a,b = [int(i) for i in entry[0].split('-')]
    l = entry[1][0]
    pw = entry[2]

    n = 0
    if pw[a-1] == l:
        n += 1
    if pw[b-1] == l:
        n += 1
    if n == 1:
        good += 1
print(good) 
