with open('day2.txt', 'r') as f:
    data = f.read()
lines = data.splitlines()
tokens = [x.split() for x in lines]
good = 0
for entry in tokens:
    a,c = entry[0].split('-')
    b = entry[2].count(entry[1][0])
    if int(a) <= b <= int(c):
        good += 1
print(good)