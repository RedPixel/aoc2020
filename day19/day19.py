def check(rules,s,seq):
    if s == '' or seq == []:
        return s == '' and seq == [] 
    
    rule = rules[seq[0]]
    if '"' in rule:
        if s[0] in rule:
            return check(rules, s[1:], seq[1:])
        else:
            return False
    else:
        return any(check(rules, s, t + seq[1:]) for t in rule)

def parse(s):
    i,rule = s.split(": ")
    if '"' not in rule:
        parts = rule.split("|")
        rule = [[int(i) for i in part.split()] for part in parts]
    return (int(i), rule)

def main():
    with open('day19b.txt', 'r') as f:
        data = f.read().split('\n\n')
    lines = data[0].splitlines()
    rules = dict(parse(rule) for rule in lines)   

    msg = data[1].splitlines()
    print(sum(check(rules, m, [0]) for m in msg))         

if __name__ == '__main__':
    main()
    