from itertools import product

dims = 3
steps = 6

# returns a tuple of the sum of the coord and delta
def new_coord(coord,delta):
    return tuple(map(sum, zip(coord, delta)))

# count how many neighbors of coord are active
def count_active_neighbors(coord,actives):
    kernel = product([-1,0,1], repeat=dims)  # (all permutations of [-1,0,1])
    neighbors = [delta != (0,) * dims and new_coord(coord,delta) for delta in kernel]
    return sum(n in actives for n in neighbors)

# generates coordinates of all neigbors of current actives (and current actives too)
def gen_neighbors(actives): 
    neighbors = set()

    for coord in actives:
        for delta in product([-1,0,1], repeat=dims): # (all permutations of [-1,0,1])
            neighbors.add(new_coord(coord,delta))
    return neighbors

def step(actives):
    potentials = gen_neighbors(actives)
    new = set()
    for c in potentials:
        count = count_active_neighbors(c,actives)
        if (c in actives and count == 2) or count == 3:
            new.add(c)
    return new


def main():
    actives = parse_input()
    
    for _ in range(steps):
        actives = step(actives)
    print(len(actives))

def parse_input():
    with open('day17.txt', 'r') as f:
        lines = f.read().splitlines()
    actives = set()

    padding = (0,)*(dims-2)
    for y,line in enumerate(lines):
        for x,char in enumerate(line):
            if(char == '#'):
                actives.add((x,y) + padding)
    return actives


if __name__ == "__main__":
    main()