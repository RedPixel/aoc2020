with open('day5.txt', 'r') as f:
    data = f.read()
passes = data.splitlines()
passes = [a.replace('B', '1') for a in passes]
passes = [a.replace('F', '0') for a in passes]
passes = [a.replace('R', '1') for a in passes]
passes = [a.replace('L', '0') for a in passes]
passes = [int(a,2) for a in passes] 
print(max(passes))
