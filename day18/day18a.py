import re

def add(m):
    a = int(m.group(1)) 
    b = int(m.group(2))
    return str(a+b)

def mult(m):
    a = int(m.group(1)) 
    b = int(m.group(2))
    return str(a*b)

def solve(expr):
    while '+' in expr or '*' in expr:
        expr = re.sub(r'^(\d+)\s*\+\s*(\d+)', add, expr)
        expr = re.sub(r'^(\d+)\s*\*\s*(\d+)', mult, expr)
    return expr
        
def parenthesis(match):
    expr = match.group(1)
    return solve(expr)

def parse(expr):
    while '(' in expr:
        expr = re.sub(r'\(([^()]+)\)', parenthesis, expr)
    return int(solve(expr))

with open('day18.txt', 'r') as f:
        lines = f.read().splitlines()
print(sum([parse(expr) for expr in lines]))
