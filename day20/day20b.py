from collections import defaultdict
from math import prod

tiles = {}
nbors = defaultdict(dict)

def find_side(side, ori_tile):
    for t in tiles:
        if t == ori_tile:
            continue
        try:
            i=tiles[t].index(side)
            return (t,i)
        except:
            pass
    return (None,None)

p = [[0 for x in range(10)] for y in range(10)] 
def puzzle():
    print(len(tiles))
    p[0][0]=(2897,1)
# 1453
# 2897
# 1439
# 2477

    # t r b l
    for x in range(1,10): # first row
        prev_id = p[0][x-1][0]
        prev_rot = p[0][x-1][1]
        print(x, prev_id, prev_rot)
        prev_r  = tiles[prev_id][1+prev_rot]
        p[0][x] = find_side(prev_r, prev_id)

        
    

# parses puzzle sides to integers
def parse_input():
    with open('day20.txt', 'r') as f:
        lines = f.read()
    curr = None
    lines = lines.replace('#','1')
    lines = lines.replace('.','0')
    ts = lines.split('\n\n')
    for tile in ts:
        lines = tile.splitlines()
        curr = int(lines[0].split(" ")[1][:-1])

        lstr,rstr="",""
        for line in lines[1:]:
            lstr = lstr + line[0]
            rstr = rstr + line[9]

        # convert to integers, min uniquely defines a side
        t = min(int(lines[1],2), int(lines[1][::-1],2))
        r = min(int(rstr, 2), int(rstr[::-1], 2))
        b = min(int(lines[10],2), int(lines[10][::-1],2))
        l = min(int(lstr, 2), int(lstr[::-1], 2))
        
        tiles[curr] = [t,r,b,l]

def main():
    parse_input()
    puzzle()

if __name__ == "__main__":
    main()