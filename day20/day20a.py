from collections import defaultdict
from math import prod

tiles = {}
nbors = defaultdict(dict)

def find_side(side, ori_tile):
    for t in tiles:
        if t == ori_tile:
            continue
        try:
            i=tiles[t].index(side)
            return (t,i)
        except:
            pass
    return (None,None)
        
def puzzle():
    for tile in tiles:
        for side in tiles[tile]:
            (t,i) = find_side(side,tile)
            if t != None:
                nbors[t][i] = tile
    for tile in nbors:
        if len(nbors[tile]) < 3:
            print(tile)
    

# parses puzzle sides to integers
def parse_input():
    with open('day20.txt', 'r') as f:
        lines = f.read()
    curr = None
    lines = lines.replace('#','1')
    lines = lines.replace('.','0')
    ts = lines.split('\n\n')
    for tile in ts:
        lines = tile.splitlines()
        curr = int(lines[0].split(" ")[1][:-1])

        lstr,rstr="",""
        for line in lines[1:]:
            lstr = lstr + line[0]
            rstr = rstr + line[9]

        # convert to integers, min uniquely defines a side
        t = min(int(lines[1],2), int(lines[1][::-1],2))
        r = min(int(rstr, 2), int(rstr[::-1], 2))
        b = min(int(lines[10],2), int(lines[10][::-1],2))
        l = min(int(lstr, 2), int(lstr[::-1], 2))
        
        tiles[curr] = [t,r,b,l]

def main():
    parse_input()
    puzzle()

if __name__ == "__main__":
    main()