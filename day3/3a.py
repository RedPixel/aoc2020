with open('day3.txt', 'r') as f:
    data = f.read()
lines = data.splitlines()

x,y = 0,0
trees = 0

for line in lines:
    if line[x] is '#':
        trees += 1
    x = (x+3) % len(line)
print(trees)