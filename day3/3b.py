def main():
    with open('day3.txt', 'r') as f:
        data = f.read()
    lines = data.splitlines()
    print(slope(lines,1,1) * 
          slope(lines,3,1) *
          slope(lines,5,1) *
          slope(lines,7,1) *
          slope(lines,1,2))

def slope(lines,dx,dy):
    x,y = 0,0
    trees = 0

    for y in range(0,len(lines),dy):
        if lines[y][x] is '#':
            trees += 1
        x = (x+dx) % len(lines[y])
    return trees

if __name__ == '__main__':
    main()