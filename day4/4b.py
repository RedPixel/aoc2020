def valid_hgt(hgt):
    unit = hgt[-2:]
    if unit == "cm":
        return 150 <= int(hgt[:-2]) <= 193
    elif unit == "in":
        return 59 <= int(hgt[:-2]) <= 76
    else:
        return False

def valid(p):
    try:
        p = p.replace("\n", " ")
        d = {k:v for k,v in (a.split(':') for a in p.split(' '))}

        return (
            1920 <= int(d.get('byr')) <= 2002 and
            2010 <= int(d.get('iyr')) <= 2020 and
            2020 <= int(d.get('eyr')) <= 2030 and
            valid_hgt(d.get('hgt')) and
            d.get('hcl')[0] == "#" and
            int(d.get('hcl')[1:],16) and
            d.get('ecl') in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'] and
            len(d.get('pid')) == 9 and
            int(d.get('pid'))
        )
        
    except:
        return False

def main():
    with open('day4.txt', 'r') as f:
        data = f.read()
    passports = data.split("\n\n")
    
    a = 0
    for p in passports:
        if valid(p):
            a += 1
    print(a)

if __name__ == '__main__':
    main()
