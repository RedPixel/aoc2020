with open('day4.txt', 'r') as f:
    data = f.read()
passports = data.split("\n\n")

a = 0
for p in passports:
    if all([a in p for a in ['byr','iyr','eyr','hgt','hcl','ecl','pid']]):
        a += 1
print(a)
