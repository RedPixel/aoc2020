def parse(data):
    poss = {}
    all_ingredients = []
    for line in data:
        parts = line.split(' (contains ')
        ingredients = parts[0].split(' ')
        all_ingredients += ingredients
        allergens = parts[1][:-1].split(', ')
        for a in allergens:
            if a not in poss:
                poss[a] = set(ingredients)
            else:
                poss[a] &= set(ingredients)
    return all_ingredients, poss

def main():
    with open('day21.txt', 'r') as f:
        data = f.read().splitlines()
    all_ingredients, poss = parse(data)
    
    found = {}
    while len(poss) > 0:
        for allergen, ingredients in poss.items():
            if len(ingredients) == 1:
                i = ingredients.pop()
                found[allergen] = i
                for ingredients in poss.values():
                    ingredients.discard(i)
        if len(ingredients)==0:
            break
        
    inv_found = {v: k for k, v in found.items()}

    print(sum(i not in inv_found for i in all_ingredients))
    canonical = sorted(inv_found, key=inv_found.get)
    for item in canonical:
        print(item, end=',')

if __name__ == "__main__":
    main()

