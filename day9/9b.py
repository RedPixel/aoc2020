with open('day9.txt', 'r') as f:
    data = f.read()
nrs = [int(nr) for nr in data.splitlines()]

target = 2089807806

for n in range(2,len(nrs)):         # n: contiguous window size
    for i in range(1,len(nrs)-n):   # slide window over nrs
        s = sum(nrs[i:i+n])
        if(s==target):
            print(min(nrs[i:i+n])+max(nrs[i:i+n]))
            return

# or as a stupid oneliner:

_=[print(min(nrs[i:i+n])+max(nrs[i:i+n])) for n in range(2,len(nrs)) for i in range(1,len(nrs)-n) if sum(nrs[i:i+n])==2089807806]
