with open('day9.txt', 'r') as f:
    data = f.read()
nrs = [int(nr) for nr in data.splitlines()]

for i in range(25,len(nrs)):
    valid = any([nrs[i]-nrs[j] in nrs[i-25:i] for j in range(i-25,i)])
    if not valid:
        print(nrs[i])