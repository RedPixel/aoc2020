nums = {7:1,12:2,1:3,0:4,16:5,2:6}
curr = 0
for n in range(7,30000000):
    if curr in nums.keys():
        tmp = n-nums[curr]
        nums[curr] = n
        curr = tmp
    else:
        nums[curr] = n
        curr = 0
print(curr)