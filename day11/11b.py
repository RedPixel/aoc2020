dirs = [(x,y) for x in range(-1,2) for y in range(-1,2) if not (x,y)==(0,0)]
print(dirs)
def count(grid,x,y):
    c = 0
    for dx,dy in dirs:
        nx,ny = x+dx,y+dy
        while 0 <= nx < len(grid) and 0 <= ny < len(grid[0]) and grid[nx][ny]=='.':
            nx,ny = nx+dx,ny+dy
        if 0 <= nx < len(grid) and 0 <= ny < len(grid[0]):
            c += grid[nx][ny] == '#'
    return c

def step(grid):
    newgrid = [['?' for _ in row] for row in grid]
    for i,row in enumerate(grid):
        for j,seat in enumerate(row):
            if grid[i][j] == 'L' and count(grid, i, j) == 0:
                newgrid[i][j] = '#'
            elif grid[i][j] == '#' and count(grid, i, j) >= 5:
                newgrid[i][j] = 'L'
            else:
                newgrid[i][j] = grid[i][j]
    return newgrid

def main():
    with open('day11.txt', 'r') as f:
        grid = [list(c) for c in f.read().splitlines()]
    
    prev = []
    while prev != grid:
        prev = grid
        grid = step(prev)
    print(sum(row.count('#') for row in grid))
    

if __name__ == "__main__":
    main()