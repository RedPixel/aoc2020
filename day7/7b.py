from parse import *

def main():
    with open('day7.txt', 'r') as f:
        rules = f.read()
    rules = rules.splitlines()


    bags = {}
    for rule in rules:
        k,v = search('{} bags contain {}.', rule)
        bags[k] = list(findall('{:d} {} bag', v))
    
    print(numbags('shiny gold')-1) # do not count the gold bag itself


def numbags(color):
    return 1 + sum(n * numbags(c) for n,c in bags[color]) 
    
if __name__ == "__main__":
    main()