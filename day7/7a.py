from parse import *

def main():
    with open('day7.txt', 'r') as f:
        rules = f.read()
    rules = rules.splitlines()


    bags = {}
    for rule in rules:
        k,v = search('{} bags contain {}.', rule)
        bags[k] = list(findall('{:d} {} bag', v))

    ok = set()
    for color in bags:
        if contains(color, 'shiny gold'):
            ok.add(color)

    print(len(ok)-1) # the gold bag can not contain itself, so -1


def contains(color, target):
    return color==target or any(contains(c,target) for _,c in bags[color])

    # ^ improved version of:

    # if color == target:
    #     return True
    # if not bags[color]:
    #     return False
    
    # for _,newcolor in bags[color]:
    #     if contains(newcolor, target):
    #         return True
    # return False
    
if __name__ == "__main__":
    main()