cups = [1,3,7,8,2,6,4,9,5]
neighbor = [i+1 for i in range(10)]
for i, num in enumerate(cups[:-1]):
    neighbor[num] = cups[i+1]
first = cups[0]
neighbor[cups[-1]] = first

for _ in range(100):
    r = neighbor[first]
    neighbor[first] = neighbor[neighbor[neighbor[r]]]
    three = r, neighbor[r], neighbor[neighbor[r]]

    d = first-1
    if d==0:
        d = len(cups)
    while d in three:
        d = d - 1
        if d==0:
            d = len(cups)

    neighbor[neighbor[neighbor[r]]] = neighbor[d]
    neighbor[d] = r
    first = neighbor[first]

cup = neighbor[1]
while cup != 1:
    print(cup, end="")
    cup = neighbor[cup]