cups = [1,3,7,8,2,6,4,9,5]
neighbor = [i+1 for i in range(1000001)]
for i, num in enumerate(cups[:-1]):
    neighbor[num] = cups[i+1]
first = cups[0]
neighbor[-1] = first
neighbor[cups[-1]] = max(cups) + 1

for i in range(10000000):
    r = neighbor[first]
    neighbor[first] = neighbor[neighbor[neighbor[r]]]
    three = r, neighbor[r], neighbor[neighbor[r]]

    d = first-1 
    if d == 0:
        d = 1000000
    while d in three:
        d=d-1
        if d == 0:
            d = 1000000

    neighbor[neighbor[neighbor[r]]] = neighbor[d]
    neighbor[d] = r
    first = neighbor[first]

cup1 = neighbor[1]
cup2 = neighbor[cup1]
print(cup1*cup2)