from parse import *

with open('day16.txt', 'r') as f:
    data = f.read()
data = data.split('\n\n')

ranges = []
for line in data[0].splitlines():
    a,b,c,d = search("{:d}-{:d} or {:d}-{:d}", line)
    ranges.append((a,b))
    ranges.append((c,d))

rate = 0
for line in data[2].splitlines()[1:]:
    for n in line.split(','):
        n = int(n)
        valid = False
        for r in ranges:
            if r[0] <= n <= r[1]:
                valid = True
                break
        if not valid:
            rate += n
print(rate)