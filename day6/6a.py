with open('day6.txt', 'r') as f:
    data = f.read()

groups = data.split("\n\n")
sum = 0
for g in groups:
    persons = g.splitlines()
    s = set()
    for p in persons:
        s.update(set(p))
    sum += len(s)
print(sum)