with open('day6.txt', 'r') as f:
    data = f.read()

groups = data.split("\n\n")
sum = 0
for g in groups:
    persons = g.splitlines()
    s = set(persons[0])
    for p in persons:
        s = s.intersection(set(p))
    sum += len(s)
print(sum)