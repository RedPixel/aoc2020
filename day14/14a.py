def andmask(mask):
    mask = mask.replace("1", "0")
    mask = mask.replace("X", "1")
    return int(mask,2)

def ormask(mask):
    mask = mask.replace("X", "0")
    return int(mask,2)
    
def main():
    with open('day14.txt', 'r') as f:
        lines = f.read().splitlines()

    mask = ""
    mem = {}
    for l in lines:
        if "mask" in l:
            mask = parse("mask = {}", l)[0]
        else:
            addr,val  = parse("mem[{:d}] = {:d}", l)
            mem[addr] = val & andmask(mask) | ormask(mask)
    print(sum(mem.values()))

if __name__ == "__main__":
    main()