def andmask2(mask):
    mask = mask.replace("0", "1")
    mask = mask.replace("X", "0")
    return int(mask,2)

def floatmem(mask, addr, val):
    i = mask.find("X")
    if i==-1:
        bmask = int(mask, 2)
        mem[addr | bmask] = val
    else:
        floatmem(mask[:i] + "0" + mask[i+1:], addr, val)
        floatmem(mask[:i] + "1" + mask[i+1:], addr, val)      

mem = {}
def main():
    with open('day14.txt', 'r') as f:
        lines = f.read().splitlines()

    mask = ""
    
    for l in lines:
        if "mask" in l:
            mask = parse("mask = {}", l)[0]
        else:
            addr,val  = parse("mem[{:d}] = {:d}", l)
            addr = addr & andmask2(mask)
            floatmem(mask,addr,val)
    print(sum(mem.values()))

if __name__ == "__main__":
    main()